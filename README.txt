The field is calculated by the following steps:

1) gmsh gemcell.geo -3 -order 2
2) ElmerGrid 14 2 gemcell.msh -autoclean
3) ElmerSolver gemcell.sif
