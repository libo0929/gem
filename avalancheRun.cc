/**
 * avalanche.cc
 *
 * For a stardard GEM foil simulation (Bo Li)
 *
 * General program flow based on example code from the Garfield++ website.
 *
 * Demonstrates electron avalanche and induced signal readout with
 * 2D finite-element visualization in Garfield++ with a LEM. 
 * LEM parameters are from:
 * C. Shalem et. al. Nucl. Instr. Meth. A, 558, 475 (2006).
 *
*/
#include <iostream>
#include <cmath>

#include <TCanvas.h>
#include <TApplication.h>
#include <TFile.h>

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/ComponentElmer.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/ViewField.hh"
#include "Garfield/Plotting.hh"
#include "Garfield/ViewFEMesh.hh"
#include "Garfield/ViewSignal.hh"
#include "Garfield/GarfieldConstants.hh"
#include "Garfield/Random.hh"
#include "Garfield/AvalancheMicroscopic.hh"

using namespace Garfield;

int main(int argc, char* argv[]) {

  TApplication app("app", &argc, argv);

  //randomEngine.Seed(123456);

  // Set relevant LEM parameters.
  // LEM thickness in cm
  const double gem_th = 0.005;      
  // Copper thickness
  const double gem_cpth = 0.0002;  
  // LEM pitch in cm
  const double gem_pitch = 0.014;   
  // X-width of drift simulation will cover between +/- axis_x
  const double axis_x = 0.02;  
  // Y-width of drift simulation will cover between +/- axis_y
  const double axis_y = 0.02;  
  const double axis_z = 0.03;


  const bool plotSignal = false;

  // Define the medium.
  MediumMagboltz* gas = new MediumMagboltz();
  // Set the temperature (K)
  gas->SetTemperature(293.15);  
  // Set the pressure (Torr)
  gas->SetPressure(740.);       
  // Allow for drifting in this medium
  gas->EnableDrift();           
  // Specify the gas mixture (Ar/CO2 70:30)
  //gas->SetComposition("ar", 1.);  
  gas->SetComposition("ar", 90., "ch4", 10.);  
  //gas->SetComposition("ar", 95., "cf4", 3., "ic4h10", 2);  
  //gas->SetComposition("ar", 70., "co2", 30.);  
  //gas->SetComposition("co2", 1.);  

  // Import an Elmer-created field map.
  ComponentElmer* elm = new ComponentElmer(
      "gemcell/mesh.header", "gemcell/mesh.elements", "gemcell/mesh.nodes",
      "gemcell/dielectrics.dat", "gemcell/gemcell.result", "cm");
  elm->EnablePeriodicityX();
  elm->EnableMirrorPeriodicityY();
  elm->SetMedium(0, gas);

  // Import the weighting field for the readout electrode.
  if(plotSignal) elm->SetWeightingField("gemcell/gemcell_WTlel.result", "wtlel");

  // Set up a sensor object.
  Sensor* sensor = new Sensor();
  sensor->AddComponent(elm);
  sensor->SetArea(-axis_x, -axis_y, -axis_z, axis_x, axis_y, axis_z);
  if(plotSignal) sensor->AddElectrode(elm, "wtlel");

  // Set the signal binning.
  const double tEnd = 500.0;
  const int nsBins = 500;
  if(plotSignal) sensor->SetTimeWindow(0., tEnd / nsBins, nsBins);

  // Create an avalanche object
  AvalancheMicroscopic* aval = new AvalancheMicroscopic();
  aval->SetSensor(sensor);
  aval->SetCollisionSteps(100);
  if(plotSignal) aval->EnableSignalCalculation();

  // Set up the object for drift line visualization.
  //ViewDrift* viewDrift = new ViewDrift();
  ViewDrift* viewDrift;
  //viewDrift->SetArea(-axis_x, -axis_y, -axis_z, axis_x, axis_y, axis_z);
  //aval->EnablePlotting(viewDrift);

  // Set the electron start parameters.
  // Starting z position for electron drift
  //const double zi = 0.5 * gem_th + gem_cpth + 0.1;  
  const double holeX = 0.0095 - 0.014 * 1/2;
  const double holeY = 0.0095 - 0.014 * sqrt(3)/2;

  const double zi = 0.015;  
  double ri = (gem_pitch / 2) * RndmUniform();
  double thetai = RndmUniform() * TwoPi;
  double xi = ri * cos(thetai);
  double yi = ri * sin(thetai);

  xi = holeX; yi = holeY;

  std::cout << " ----- > " << xi << ", " << yi << std::endl;

  // Create ROOT histograms of the signal and a file in which to store them.
  TFile* f = new TFile("avalanche_signals.root", "RECREATE");
  //TH1F* hS = new TH1F("hh", "hh", nsBins, 0, tEnd);        // total signal
  TH1F* hS = new TH1F("hh", "hh", nsBins, -axis_z - 0.002, zi + 0.002);        // total signal
  TH1F* hInt = new TH1F("hInt", "hInt", nsBins, 0, tEnd);  // integrated signal

  for(int i = 0; i < 200; ++i)
  {
  // Calculate the avalanche.
  aval->AvalancheElectron(xi, yi, zi, 0., 0., 0., 0., 0.);
  std::cout << "... electron " << i << " avalanche complete with "
            << aval->GetNumberOfElectronEndpoints() << " electron tracks.\n";

  for(int ie = 0; ie < aval->GetNumberOfElectronEndpoints(); ++ie)
  {
	  double x0, y0, z0, t0, e0, x1, y1, z1, t1, e1;
	  int status;

	  aval->GetElectronEndpoint(ie, x0, y0, z0, t0, e0, x1, y1, z1, t1, e1, status);

	  hS->Fill(z1);
  }

  // Extract the calculated signal.
  double bscale = tEnd / nsBins;  // time per bin
  double sum = 0.;  // to keep a running sum of the integrated signal


  // Fill the histograms with the signals.
  //  Note that the signals will be in C/(ns*binWidth), and we will divide by e
  // to give a signal in e/(ns*binWidth).
  //  The total signal is then the integral over all bins multiplied by the bin
  // width in ns.
  for (int i = 0; i < nsBins; i++) {
    double wt = sensor->GetSignal("wtlel", i) / ElementaryCharge;
    sum += wt;
    //hS->Fill(i * bscale, wt);
    hInt->Fill(i * bscale, sum);
  }

  if(plotSignal) std::cout << "total charge: " << sum << std::endl;



  // Plot the signal.
  if (plotSignal) {
    TCanvas* cSignal = new TCanvas("signal", "Signal");
    ViewSignal* vSignal = new ViewSignal();
    vSignal->SetSensor(sensor);
    vSignal->SetCanvas(cSignal);
    vSignal->PlotSignal("wtlel");
  }

#if 0
  // Plot the geometry, field and drift lines.
  TCanvas* cGeom = new TCanvas("geom", "Geometry/Avalanche/Fields");
  cGeom->SetLeftMargin(0.14);
  const bool plotContours = false;
  if (plotContours) {
    ViewField* vf = new ViewField();
    vf->SetSensor(sensor);
    vf->SetCanvas(cGeom);
    vf->SetArea(-axis_x, -axis_y, axis_x, axis_y);
	vf->SetVoltageRange(-600, 50);
    vf->SetNumberOfContours(35);
    vf->SetNumberOfSamples2d(800, 800);
    vf->SetPlane(0, 1, 0, 0, 0, 0);
    vf->PlotContour("v");
  }

  // Set up the object for FE mesh visualization.
  ViewFEMesh* vFE = new ViewFEMesh();
  vFE->SetArea(-axis_x, -axis_z, -axis_y, axis_x, axis_z, axis_y);
  vFE->SetCanvas(cGeom);
  vFE->SetComponent(elm);
  vFE->SetPlane(0, -1, 0, 0, holeY, 0);
  //vFE->SetPlane(0, -0, 1, 0, 0, 0);
  vFE->SetFillMesh(true);
  vFE->SetColor(1, kGray);
  vFE->SetColor(2, kYellow + 3);
  vFE->SetColor(3, kYellow + 3);

  if (!plotContours) {
    vFE->EnableAxes();
    vFE->SetXaxisTitle("x (cm)");
    vFE->SetYaxisTitle("z (cm)");
    vFE->SetViewDrift(viewDrift);
    vFE->Plot();
  }

  app.Run(kTRUE);
#endif

  }
  // Write the histograms to the TFile.
  hS->Write();
  hInt->Write();
  f->Close();

  return 0;
}
