// change micron to centimeter
mum2cm = 0.0001;                  

// height of dielectrics
hd  = 50. * mum2cm;             
// height of copper 
hc  = 5.    * mum2cm;             
// diameter of hole at center plane
dhc = 60.   * mum2cm;             
// diameter of hole at surface
dhs = 70.   * mum2cm;             

//electrode
ztop = 500 * mum2cm + hd/2 + hc;
zbottom = 500 * mum2cm + hd/2 + hc;

//pitch
pitch = 140. * mum2cm;
hpch  = pitch/2;
pitch2 = pitch * 0.866025403784439;

// half of hd
hhd = hd/2;             

//copper etching
//ce = hc*(dhs-dhc)/hhd/2;
ce = 0.;

//Printf("ce = %g", ce);
// Characteristic lengths
// hole region
//lch = 8.    * mum2cm;             
// drift region
//lcd = 15.   * mum2cm;             

//lch = 7 * mum2cm;
lch = 5 * mum2cm;
lcd = 30 * mum2cm;

// radius
rhc = dhc/2;
rhs = dhs/2;

point1 = newp; Point(point1) = {-hpch, -hpch, 0, lch}; 
point2 = newp; Point(point2) = {-hpch+rhc, -hpch, 0, lch};
point3 = newp; Point(point3) = {-hpch, -hpch+rhc, 0, lch}; 

point4 = newp; Point(point4) = {-hpch,  -hpch, hhd, lch};
point5 = newp; Point(point5) = {-hpch+rhs, -hpch, hhd, lch};
point6 = newp; Point(point6) = {-hpch, -hpch+rhs, hhd, lch}; 


line1 = newc; Circle(line1) = {point2, point1, point3};
line2 = newc; Circle(line2) = {point5, point4, point6};

line3 = newc; Line(line3) = {point3, point6};
line4 = newc; Line(line4) = {point2, point5};

line5 = newl; Line(line5) = {point3, point1};
line6 = newl; Line(line6) = {point6, point4};
line7 = newl; Line(line7) = {point1, point4};
line8 = newl; Line(line8) = {point4, point5};
line9 = newl; Line(line9) = {point1, point2};

ll1 = newl; Line Loop(ll1) = {line6, line8, line2};
ll2 = newl; Line Loop(ll2) = {line1, line5, line9};
ll3 = newl; Line Loop(ll3) = {line6, -line7, -line5, line3};
ll4 = newl; Line Loop(ll4) = {line8, -line4, -line9, line7};
ll5 = newl; Line Loop(ll5) = {line2, -line3, -line1, line4};

sur1 = news; Plane Surface(sur1) = {ll1};
sur2 = news; Plane Surface(sur2) = {ll2};
sur3 = news; Plane Surface(sur3) = {ll3};
sur4 = news; Plane Surface(sur4) = {ll4};
sur5 = news; Ruled Surface(sur5) = {ll5};

out[] = Symmetry {0, 0, 1, 0} { Duplicata{ Surface{sur1, sur3, sur4, sur5}; } };
surl1 = news; Surface Loop(surl1) = {sur1, sur3, sur4, sur5, out[]};

//FIXME!!!!!!!
//Translate {-0.0007, 0, 0} {
//  Point{12};
//}
//Translate {0, -0.0007, 0} {
//  Point{7};
//}

vol1 = newv; Volume(vol1) = {surl1};
vol2 = Translate {0, pitch2, 0} { Duplicata{ Volume{vol1}; } };
Rotate {{0, 0, 1}, {-pitch/2, -pitch/2+pitch2, 0}, Pi} { Volume{vol2}; }
//Rotate {{0, 0, 1}, {0, pitch/2, 0}, Pi} { Volume{vol2}; }
Translate {pitch/2, 0, 0} { Volume{vol2}; }
//Translate {0, -pitch/2+pitch2/2, 0} { Volume{vol2}; }

point7 = newp; Point(point7) = {-hpch, pitch2-pitch/2, hhd, lch};
point8 = newp; Point(point8) = {-hpch, pitch2-pitch/2, -hhd, lch};

point9 = newp; Point(point9)   = {0, -hpch, hhd, lch};
point10 = newp; Point(point10) = {0, -hpch, -hhd, lch};

Line(72) = {84, 13};
Line(73) = {85, 76};
Line(74) = {84, 85};
Line(75) = {12, 85};
Line(76) = {5, 84};
Line(77) = {6, 82};
Line(78) = {83, 82};
Line(79) = {82, 18};
Line(80) = {83, 81};
Line(81) = {7, 83};

Translate {pitch, 0, 0} { Duplicata { Volume{36}; } }
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} { Volume{82}; }
Translate {0, -pitch, 0} { Volume{82}; }
Symmetry {1, 0, 0, 0} { Duplicata { Volume{vol2}; } }

point11 = newp; Point(point11) = {hpch, pitch2-pitch/2, hhd, lch};
point12 = newp; Point(point12) = {hpch, pitch2-pitch/2, -hhd, lch};

Line(152) = {91, 224};
Line(153) = {154, 225};
Line(154) = {224, 225};
Line(155) = {223, 225};
Line(156) = {160, 224};
Line(157) = {84, 86};
Line(158) = {85, 149};

cex = -hpch+rhs+ce;
cey = -hpch;
cez = hhd+hc;

//upper copper
pointc1 = newp; Point(pointc1) = {cex, cey, cez, lch};
pointc2 = newp; Point(pointc2) = {cey, cex, cez, lch};
pointc3 = newp; Point(pointc3) = {cey, cey, cez, lch};
pointc4 = newp; Point(pointc4) = {-cex, cey, cez, lch};
pointc5 = newp; Point(pointc5) = {-cey, cex, cez, lch};
pointc6 = newp; Point(pointc6) = {-cey, cey, cez, lch};
//FIXME
pointc7 = newp; Point(pointc7) = {0, pitch2-pitch/2, cez, lch};
pointc8 = newp; Point(pointc8) = {-cex-hpch, pitch2-pitch/2, cez, lch};
pointc9 = newp; Point(pointc9) = {hpch+cex, pitch2-pitch/2, cez, lch};
pointc10 = newp; Point(pointc10) = {0, pitch2-pitch/2-rhs-ce, cez, lch};
pointc11 = newp; Point(pointc11) = {-hpch, pitch2-pitch/2, cez, lch};
pointc12 = newp; Point(pointc12) = {hpch, pitch2-pitch/2, cez, lch};

//FIXME!!!!!!!!!!!!!!
//cex = -hpch+rhs-ce-7*mum2cm;
cex = -hpch+rhs-ce;

//bottom copper
pointcb1 = newp; Point(pointcb1) = {cex, cey, -cez, lch};
pointcb2 = newp; Point(pointcb2) = {cey, cex, -cez, lch};
pointcb3 = newp; Point(pointcb3) = {cey, cey, -cez, lch};
pointcb4 = newp; Point(pointcb4) = {-cex, cey, -cez, lch};
pointcb5 = newp; Point(pointcb5) = {-cey, cex, -cez, lch};
pointcb6 = newp; Point(pointcb6) = {-cey, cey, -cez, lch};
pointcb7 = newp; Point(pointcb7) = {0, pitch2-pitch/2, -cez, lch};
pointcb8 = newp; Point(pointcb8) = {-cex-hpch, pitch2-pitch/2, -cez, lch};
pointcb9 = newp; Point(pointcb9) = {hpch+cex, pitch2-pitch/2, -cez, lch};
pointcb10 = newp; Point(pointcb10) = {0, pitch2-pitch/2-rhs, -cez, lch};
pointcb11 = newp; Point(pointcb11) = {-hpch, pitch2-pitch/2, -cez, lch};
pointcb12 = newp; Point(pointcb12) = {hpch, pitch2-pitch/2, -cez, lch};

pet1 = newp; Point(pet1) = {-hpch, -hpch, ztop, lcd};
pet2 = newp; Point(pet2) = {-hpch,  pitch2-pitch/2, ztop, lcd};
pet3 = newp; Point(pet3) = {hpch,  -hpch, ztop, lcd};
pet4 = newp; Point(pet4) = {hpch,   pitch2-pitch/2, ztop, lcd};
pet5 = newp; Point(pet5) = {0,      -hpch, ztop, lcd};
pet6 = newp; Point(pet6) = {0,      pitch2-pitch/2, ztop, lcd};

peb1 = newp; Point(peb1) = {-hpch, -hpch, -zbottom, lcd};
peb2 = newp; Point(peb2) = {-hpch,  pitch2-pitch/2, -zbottom, lcd};
peb3 = newp; Point(peb3) = {hpch,  -hpch, -zbottom, lcd};
peb4 = newp; Point(peb4) = {hpch,   pitch2-pitch/2, -zbottom, lcd};
peb5 = newp; Point(peb5) = {0,     -hpch, -zbottom, lcd};
peb6 = newp; Point(peb6) = {0,      pitch2-pitch/2, -zbottom, lcd};


//point of upper copper at zero
pucz = newp; Point(pucz) = {0, -hpch, cez, lch};
//point of lower copper at zero
plcz = newp; Point(plcz) = {0, -hpch, -cez, lch};

//
Delete { Volume{36, 82, 117, 37}; }

Delete { Surface{15}; }
Delete { Surface{20}; }
Delete { Surface{16}; }
Delete { Surface{83}; }
Delete { Surface{102}; }
Delete { Surface{118}; }
Delete { Surface{38}; }
Delete { Surface{137}; }
Delete { Surface{57}; }

Line(159) = {237, 224};
Line(160) = {237, 234};
Line(161) = {234, 232};
Line(162) = {232, 14};
Line(163) = {232, 233};
Line(164) = {233, 236};
Line(165) = {236, 82};
Line(166) = {233, 18};
Line(167) = {234, 160};
Line(168) = {235, 13};
Line(169) = {76, 247};
Line(170) = {223, 246};
Line(171) = {77, 244};
Line(172) = {81, 245};
Line(173) = {83, 248};
Line(174) = {244, 245};
Line(175) = {246, 244};
Line(176) = {245, 248};
Line(177) = {239, 248};
Line(178) = {240, 239};
Line(179) = {7, 239};
Line(180) = {238, 12};
Line(181) = {240, 238};
Line(182) = {240, 8};
Line(183) = {238, 263};
Line(184) = {263, 241};
Line(185) = {241, 149};
Line(186) = {241, 243};
Line(187) = {243, 242};
Line(188) = {242, 154};
Line(189) = {87, 231};
Line(190) = {86, 229};
Line(191) = {91, 230};
Line(192) = {84, 262};
Line(193) = {5, 226};
Line(194) = {4, 228};
Line(195) = {227, 6};
Line(196) = {235, 262};
Line(197) = {236, 227};
Line(198) = {227, 228};
Line(199) = {226, 228};
Line(200) = {226, 262};
Line(201) = {262, 229};
Line(202) = {229, 231};
Line(203) = {231, 230};
Line(204) = {230, 237};
Line(205) = {232, 235};
Line(206) = {247, 244};
Line(207) = {246, 249};
Line(208) = {249, 225};
Line(209) = {249, 242};
Line(210) = {243, 150};
Line(211) = {263, 85};
Line(212) = {247, 263};
Line(213) = {250, 228};
Line(214) = {262, 254};
Line(215) = {252, 231};
Line(216) = {253, 237};
Line(217) = {232, 255};
Line(218) = {251, 236};
Line(219) = {251, 255};
Line(220) = {255, 253};
Line(221) = {253, 252};
Line(222) = {252, 254};
Line(223) = {254, 250};
Line(224) = {250, 251};
Line(225) = {257, 248};
Line(226) = {244, 261};
Line(227) = {259, 249};
Line(228) = {256, 240};
Line(229) = {263, 260};
Line(230) = {258, 243};
Line(231) = {258, 259};
Line(232) = {259, 261};
Line(233) = {261, 257};
Line(234) = {257, 256};
Line(235) = {256, 260};
Line(236) = {260, 258};
Circle(237) = {241, 243, 242};
Circle(238) = {239, 240, 238};
Circle(239) = {247, 244, 245};
Circle(240) = {247, 244, 246};
Circle(241) = {233, 232, 235};
Circle(242) = {235, 232, 234};
Circle(243) = {226, 228, 227};
Circle(244) = {230, 231, 229};
Line Loop(245) = {219, 220, 221, 222, 223, 224};
Plane Surface(246) = {245};
Line Loop(247) = {223, 213, -199, 200, 214};
Plane Surface(248) = {247};
Line Loop(249) = {214, -222, 215, -202, -201};
Plane Surface(250) = {249};
Line Loop(251) = {215, 203, 204, -216, 221};
Plane Surface(252) = {251};
Line Loop(253) = {220, 216, 160, 161, 217};
Plane Surface(254) = {253};
Line Loop(255) = {217, -219, 218, -164, -163};
Plane Surface(256) = {255};
Line Loop(257) = {224, 218, 197, 198, -213};
Plane Surface(258) = {257};
Line Loop(259) = {235, 236, 231, 232, 233, 234};
Plane Surface(260) = {259};
Line Loop(261) = {233, 225, -176, -174, 226};
Plane Surface(262) = {261};
Line Loop(263) = {226, -232, 227, -207, 175};
Plane Surface(264) = {263};
Line Loop(265) = {234, 228, 178, 177, -225};
Plane Surface(266) = {265};
Line Loop(267) = {231, 227, 209, -187, -230};
Plane Surface(268) = {267};
Line Loop(269) = {184, 186, -230, -236, -229};
Plane Surface(270) = {269};
Line Loop(271) = {235, -229, -183, -181, -228};
Plane Surface(272) = {271};
Line Loop(273) = {177, -173, -81, 179};
Plane Surface(274) = {273};
Line Loop(275) = {176, -173, 80, 172};
Plane Surface(276) = {275};
Line Loop(277) = {207, 208, -155, 170};
Plane Surface(278) = {277};
Line Loop(279) = {209, 188, 153, -208};
Plane Surface(280) = {279};
Line Loop(281) = {184, 185, -158, -211};
Plane Surface(282) = {281};
Line Loop(283) = {183, 211, -75, -180};
Plane Surface(284) = {283};
Line Loop(285) = {28, 81, 78, -77, -3};
Plane Surface(286) = {285};
Line Loop(287) = {80, 68, -49, -79, -78};
Plane Surface(288) = {287};
Line Loop(289) = {156, 154, -155, 148, -129};
Plane Surface(290) = {289};
Line Loop(291) = {164, 165, 79, -166};
Plane Surface(292) = {291};
Line Loop(293) = {197, 195, 77, -165};
Plane Surface(294) = {293};
Line Loop(295) = {198, -194, -6, -195};
Plane Surface(296) = {295};
Line Loop(297) = {199, -194, 8, 193};
Plane Surface(298) = {297};
Line Loop(299) = {200, -192, -76, 193};
Plane Surface(300) = {299};
Line Loop(301) = {76, 74, -75, 31, 4};
Plane Surface(302) = {301};
Line Loop(303) = {201, -190, -157, 192};
Plane Surface(304) = {303};
Line Loop(305) = {157, -91, 110, -158, -74};
Plane Surface(306) = {305};
Line Loop(307) = {202, -189, -84, 190};
Plane Surface(308) = {307};
Line Loop(309) = {103, -210, -186, 185};
Plane Surface(310) = {309};
Line Loop(311) = {104, -188, -187, 210};
Plane Surface(312) = {311};
Line Loop(313) = {203, -191, -85, 189};
Plane Surface(314) = {313};
Line Loop(315) = {204, 159, -152, 191};
Plane Surface(316) = {315};
Line Loop(317) = {94, -113, 153, -154, -152};
Plane Surface(318) = {317};
Line Loop(319) = {167, -120, -162, -161};
Plane Surface(320) = {319};
Line Loop(321) = {163, 166, -40, -162};
Plane Surface(322) = {321};
Line Loop(323) = {139, 170, 175, -171};
Plane Surface(324) = {323};
Line Loop(325) = {171, 174, -172, -59};
Plane Surface(326) = {325};
Line Loop(327) = {179, -178, 182, -21};
Plane Surface(328) = {327};
Line Loop(329) = {182, 22, -180, -181};
Plane Surface(330) = {329};
Line Loop(331) = {185, -105, -188, -237};
Ruled Surface(332) = {331};
Line Loop(333) = {86, 190, -244, -191};
Ruled Surface(334) = {333};
Line Loop(335) = {238, 180, 23, 179};
Ruled Surface(336) = {335};
Line Loop(337) = {243, 195, -2, 193};
Ruled Surface(338) = {337};
Line Loop(339) = {168, -121, -167, -242};
Ruled Surface(340) = {339};
Line Loop(341) = {166, 41, -168, -241};
Ruled Surface(342) = {341};
Line Loop(343) = {170, -240, -169, -140};
Ruled Surface(344) = {343};
Line Loop(345) = {169, 239, -172, 60};
Ruled Surface(346) = {345};
Line Loop(347) = {156, -159, 160, 167};
Plane Surface(348) = {347};
Line Loop(349) = {243, -197, -164, 241, 196, -200};
Plane Surface(350) = {349};
Line Loop(351) = {196, 201, -244, 204, 160, -242};
Plane Surface(352) = {351};
Line Loop(353) = {77, 79, 41, -72, -76, 2};
Plane Surface(354) = {353};
Line Loop(355) = {72, -121, 156, -152, 86, -157};
Plane Surface(356) = {355};
Line Loop(357) = {73, -140, 155, -153, 105, -158};
Plane Surface(358) = {357};
Line Loop(359) = {212, 184, 237, -209, -207, -240};
Plane Surface(360) = {359};
Line Loop(361) = {75, 73, -60, -80, -81, -23};
Plane Surface(362) = {361};
Line Loop(363) = {177, -176, -239, 212, -183, -238};
Plane Surface(364) = {363};
Surface Loop(365) = {256, 254, 246, 252, 250, 248, 258, 296, 298, 18, 29, 330, 328, 266, 260, 272, 270, 310, 106, 111, 312, 268, 264, 262, 326, 324, 146, 127, 320, 322, 47, 66, 92, 314, 308, 87, 24, 17, 132, 151, 340, 342, 350, 338, 352, 334, 52, 71, 344, 360, 364, 346, 336, 332, 19, 34, 97, 116};
Volume(366) = {365};
Surface Loop(367) = {286, 288, 19, 34, 302, 306, 97, 116, 318, 290, 132, 151, 52, 71, 354, 356, 362, 358};
Volume(368) = {367};
Surface Loop(369) = {348, 316, 340, 342, 350, 338, 352, 334, 292, 294, 300, 304, 356, 354};
Volume(370) = {369};
Surface Loop(371) = {274, 276, 346, 344, 360, 364, 336, 332, 278, 280, 282, 284, 358, 362};
Volume(372) = {371};
Physical Surface(373) = {352, 350, 316, 334, 304, 300, 338, 294, 292, 342, 340, 348, 356, 354};
Physical Surface(374) = {362, 364, 274, 276, 336, 284, 282, 332, 280, 278, 344, 346, 358, 360};
Physical Surface(375) = {246};
Physical Surface(376) = {260};
Physical Surface(377) = {258, 286, 17, 296, 24, 328, 266};
Physical Surface(378) = {268, 252, 92, 314, 111, 312, 318};
Physical Surface(379) = {248, 302, 298, 18, 29, 330, 272};
Physical Surface(380) = {320, 127, 146, 324, 254, 264, 290};
Physical Surface(381) = {270, 87, 308, 106, 310, 250, 306};
Physical Surface(382) = {288, 47, 66, 322, 326, 256, 262};
Physical Volume(383) = {366};
Physical Volume(384) = {368};

Physical Volume(385) = {370};
Physical Volume(386) = {372};

//FIXME!!!!!!!!
Translate {0.0165, 0.0165, 0} {
  Volume{366, 368, 370, 372};
}

